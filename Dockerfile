# ubuntu-packer-qemu

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04
ARG PACKER_VERSION=1.6.6


FROM ${DOCKER_REGISTRY_URL}ubuntu:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-packer-qemu

ARG PACKER_VERSION
ARG PACKER_ARCHIVE=packer_${PACKER_VERSION}_linux_amd64.zip
ARG PACKER_URL=https://releases.hashicorp.com/packer/${PACKER_VERSION}/${PACKER_ARCHIVE}

WORKDIR /tmp

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
		curl \
		dosfstools \
		genisoimage \
		make \
		mtools \
		qemu-system-x86 \
		qemu-utils \
		unzip \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete

COPY ./cache/pkg/ ./

RUN \
	[ -f ./${PACKER_ARCHIVE} ] || curl -Lo ./${PACKER_ARCHIVE} ${PACKER_URL} ; \
	unzip -o ./${PACKER_ARCHIVE} -d /usr/local/bin/ ; \
	rm ./${PACKER_ARCHIVE}

WORKDIR /

RUN \
	packer version
