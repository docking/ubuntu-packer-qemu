# ubuntu-packer-qemu

A Docker image based on Ubuntu including Packer & QEMU.

Tools:
 - Packer
 - QEMU
 - DOS/FAT tools (dosfstools + mtools)
 - ISO tools (genisoimage)

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the container image: `make build`
* Try the container: `make shell`
