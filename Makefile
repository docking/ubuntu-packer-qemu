# ubuntu-packer-qemu

all: help

.PHONY: all help build shell

help:
	@echo "Available targets:"
	@echo
	@echo "  build      build and tag Docker image"
	@echo "  shell      run a shell in Docker image"
	@echo


CUSTOM_VERSION?=1.0
UBUNTU_VERSION?=18.04
PACKER_VERSION?=1.6.6

DOCKER_IMAGE?=ubuntu-packer-qemu
DOCKER_TAG?=$(CUSTOM_VERSION)-$(UBUNTU_VERSION)-$(PACKER_VERSION)

DOCKER_USER:=packer
DOCKER_HOME:=/$(USER)
DOCKER_UID:=$(shell id -u)
DOCKER_GID:=$(shell test -e /dev/kvm && stat -c "%g" /dev/kvm || id -g)

DOCKER_BUILD:=docker build \
	$(if $(DOCKER_NO_CACHE),--no-cache,) \
	--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
	--build-arg PACKER_VERSION=$(PACKER_VERSION) \

DOCKER_RUN:=docker run \
	--cap-add NET_ADMIN \
	--device=/dev/kvm:/dev/kvm \
	--device=/dev/net/tun:/dev/net/tun \
	--env HOME=$(DOCKER_HOME) \
	--env USER=$(DOCKER_USER) \
	--user $(DOCKER_UID):$(DOCKER_GID) \
	--volume $(PWD):$(DOCKER_HOME) \
	--workdir $(DOCKER_HOME) \

BUILD_DIR:=./build
CACHE_DIR:=./cache

build:
	mkdir -p $(CACHE_DIR)/pkg
	$(DOCKER_BUILD) \
		--tag $(DOCKER_IMAGE):$(DOCKER_TAG) \
		.

shell:
	$(DOCKER_RUN) \
		--interactive --tty \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		bash


# $(1): file
# $(2): url
# $(3): sha256sum
define Download
	mkdir -p $(dir $(1))
	curl -Lo $(1).dl --retry 3 $(2) ; \
	SHA256SUM="$$(cat $(1).dl | sha256sum | cut -d ' ' -f 1 )" ; \
	if [ "$${SHA256SUM}" != "$(3)" ]; then \
		echo "ERROR: SHA256 digest of '$(1)' is wrong! It is $${SHA256SUM}, while it should be $(3)" ; exit 1 ; \
	fi
	mv -f $(1).dl $(1)
endef

PACKER_ARCHIVE:=packer_$(PACKER_VERSION)_linux_amd64.zip
PACKER_ARCHIVE_SHA256SUM:=a20ec68e9eb6e1d6016481003f705babbecc28e234f8434f3a35f675cb200ea8
PACKER_ARCHIVE_URL:=https://releases.hashicorp.com/packer/$(PACKER_VERSION)/$(PACKER_ARCHIVE)

CENTOS_CONFIG_ISO:=CentOS-7-cloudinit.iso
CENTOS_SOURCE_IMG:=CentOS-7-x86_64-GenericCloud-2003.qcow2c
CENTOS_SOURCE_IMG_SHA256SUM:=b7555ecf90b24111f2efbc03c1e80f7b38f1e1fc7e1b15d8fee277d1a4575e87
CENTOS_SOURCE_IMG_URL:=https://cloud.centos.org/centos/7/images/$(CENTOS_SOURCE_IMG)
CENTOS_TARGET_IMG:=CentOS-7-custom-x86_64.img

UBUNTU_CONFIG_ISO:=ubuntu-18.04-server-cloudinit.iso
UBUNTU_SOURCE_IMG:=ubuntu-18.04-server-cloudimg-amd64.img
UBUNTU_SOURCE_IMG_SHA256SUM:=3b2e3aaaebf2bc364da70fbc7e9619a7c0bb847932496a1903cd4913cf9b1a26
UBUNTU_SOURCE_IMG_URL:=https://cloud-images.ubuntu.com/releases/bionic/release-20200506/$(UBUNTU_SOURCE_IMG)
UBUNTU_TARGET_IMG:=ubuntu-18.04-custom-amd64.img

$(CACHE_DIR)/img/$(CENTOS_CONFIG_ISO): ./tests/centos/meta-data ./tests/centos/user-data
	tools/gen-cidata-iso $@ $^

$(CACHE_DIR)/img/$(CENTOS_SOURCE_IMG):
	$(call Download,$@,$(CENTOS_SOURCE_IMG_URL),$(CENTOS_SOURCE_IMG_SHA256SUM))

$(CACHE_DIR)/img/$(UBUNTU_CONFIG_ISO): ./tests/ubuntu/meta-data ./tests/ubuntu/user-data
	tools/gen-cidata-iso $@ $^

$(CACHE_DIR)/img/$(UBUNTU_SOURCE_IMG):
	$(call Download,$@,$(UBUNTU_SOURCE_IMG_URL),$(UBUNTU_SOURCE_IMG_SHA256SUM))

$(CACHE_DIR)/pkg/$(PACKER_ARCHIVE):
	$(call Download,$@,$(PACKER_ARCHIVE_URL),$(PACKER_ARCHIVE_SHA256SUM))

PACKER_BUILD:= \
	PACKER_CACHE_DIR="$(BUILD_DIR)/cache" \
	PACKER_LOG="$(PACKER_LOG)" \
	packer build \
		-on-error="ask" \
		-var headless="$(if $(PACKER_HEADLESS),$(PACKER_HEADLESS),true)" \

$(BUILD_DIR)/$(CENTOS_TARGET_IMG): ./tests/ubuntu.json $(CACHE_DIR)/img/$(CENTOS_CONFIG_ISO) $(CACHE_DIR)/img/$(CENTOS_SOURCE_IMG)
	$(PACKER_BUILD) \
		-var config_iso="$(CACHE_DIR)/img/$(CENTOS_CONFIG_ISO)" \
		-var source_img="$(CACHE_DIR)/img/$(CENTOS_SOURCE_IMG)" \
		-var source_checksum="$(CENTOS_SOURCE_IMG_SHA256SUM)" \
		-var target_img="$(BUILD_DIR)/$(CENTOS_TARGET_IMG)" \
		./tests/centos.json

$(BUILD_DIR)/$(UBUNTU_TARGET_IMG): ./tests/ubuntu.json $(CACHE_DIR)/img/$(UBUNTU_CONFIG_ISO) $(CACHE_DIR)/img/$(UBUNTU_SOURCE_IMG)
	$(PACKER_BUILD) \
		-var config_iso="$(CACHE_DIR)/img/$(UBUNTU_CONFIG_ISO)" \
		-var source_img="$(CACHE_DIR)/img/$(UBUNTU_SOURCE_IMG)" \
		-var source_checksum="$(UBUNTU_SOURCE_IMG_SHA256SUM)" \
		-var target_img="$(BUILD_DIR)/$(UBUNTU_TARGET_IMG)" \
		./tests/ubuntu.json


.PHONY: build-centos build-centos-in-docker

build-centos: $(BUILD_DIR)/$(CENTOS_TARGET_IMG)

build-centos-in-docker:
	$(DOCKER_RUN) \
		-e PACKER_LOG="$(PACKER_LOG)" \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(MAKE) build-centos


.PHONY: build-ubuntu build-ubuntu-in-docker

build-ubuntu: $(BUILD_DIR)/$(UBUNTU_TARGET_IMG)

build-ubuntu-in-docker:
	$(DOCKER_RUN) \
		-e PACKER_LOG="$(PACKER_LOG)" \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(MAKE) build-ubuntu


.PHONY: cache cache-build cache-tests

cache-build: $(CACHE_DIR)/pkg/$(PACKER_ARCHIVE)

cache-tests: $(CACHE_DIR)/img/$(CENTOS_CONFIG_ISO) $(CACHE_DIR)/img/$(CENTOS_SOURCE_IMG)
cache-tests: $(CACHE_DIR)/img/$(UBUNTU_CONFIG_ISO) $(CACHE_DIR)/img/$(UBUNTU_SOURCE_IMG)

cache: cache-build cache-tests
